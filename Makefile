clean:
	find . -name "*.pyc" -exec rm -rf {} \;

run: clean
	python lotus_back/manage.py runserver 0.0.0.0:8000

migrate:
	python lotus_back/manage.py migrate

migrations:
	python lotus_back/manage.py makemigrations

install_requirements:
	pip install -r lotus_back/requirements.txt

user:
	python lotus_back/manage.py createsuperuser


shell: clean
	python lotus_back/manage.py shell

deploy: install_requirements migrate clean
