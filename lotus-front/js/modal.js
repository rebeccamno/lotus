Vue.component('modal', {
    props: [
      'title',
      'images',
    ],
    methods: {
        closeModal(e){
            if(Boolean(this.close)){
              this.close();
            }
        }
    },
    template:
    `
    <transition name="modal">
        <div class="modal-mask">
            <div @click="$emit('close')" class="modal-wrapper">
                <div class="modal-container"  @click.stop>
                    <div class="modal-header">
                        <h1>{{ title }}</h1>
                        <i @click="$emit('close')" class="close fas fa-times"></i>
                    </div>
                    <div class="modal-body">
                        <slot name="images-slider">
                        </slot>
                        <slot name="body">
                        </slot>
                        <slot name="testimonial-slider">
                        </slot>
                    </div>
                </div>
            </div>
        </div>
    </transition>
    `
})
