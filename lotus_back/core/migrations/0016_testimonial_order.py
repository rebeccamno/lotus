# Generated by Django 2.2.5 on 2019-10-16 01:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_auto_20191015_2246'),
    ]

    operations = [
        migrations.AddField(
            model_name='testimonial',
            name='order',
            field=models.IntegerField(default=0),
        ),
    ]
