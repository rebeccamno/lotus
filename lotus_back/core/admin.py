from django.contrib import admin

from .models import (Banner, Feature, FeatureImage, FeaturePlace, FeaturePrice,
                     FeatureTestimonialImage, Testimonial, TripDate, TripImage,
                     TripIncludedItem, TripInfo, TripPrice,
                     TripTestimonialImage, WebsiteInfo, Subscribe)


class FeaturePlaceInline(admin.TabularInline):
    model = FeaturePlace
    extra = 1


class FeaturePriceInline(admin.TabularInline):
    model = FeaturePrice
    extra = 1


class FeatureImageInline(admin.TabularInline):
    model = FeatureImage
    extra = 1


class FeatureTestimonialImageInline(admin.TabularInline):
    model = FeatureTestimonialImage
    extra = 1


class WebsiteInfoAdmin(admin.ModelAdmin):

    list_display = ['website_name', ]
    fieldsets = (
        (None, {'fields': ('website_name', 'history', 'mission', 'values',
                           'cnpj', 'razao_social', 'facebook', 'instagram', 'address', 'phones', 'contact_image')}),
    )


class FeatureAdmin(admin.ModelAdmin):

    list_display = ['name', ]
    search_fields = ['name', ]

    inlines = [
        FeatureImageInline,
        FeaturePriceInline,
        FeaturePlaceInline,
        FeatureTestimonialImageInline
    ]


class TripInfoTestimonialImageInline(admin.TabularInline):
    model = TripTestimonialImage
    extra = 1


class TripInfoImageInline(admin.TabularInline):
    model = TripImage
    extra = 1


class TripPriceInline(admin.TabularInline):
    model = TripPrice
    extra = 1


class TripDateInline(admin.TabularInline):
    model = TripDate
    extra = 1


class TripIncludedItemInline(admin.TabularInline):
    model = TripIncludedItem
    extra = 1


class TripInfoAdmin(admin.ModelAdmin):

    list_display = ['title', 'place']
    search_fields = ['title', ]

    inlines = [
        TripDateInline,
        TripIncludedItemInline,
        TripInfoImageInline,
        TripInfoTestimonialImageInline
    ]


admin.site.register(WebsiteInfo, WebsiteInfoAdmin)
admin.site.register(Feature, FeatureAdmin)
admin.site.register(TripInfo, TripInfoAdmin)
admin.site.register(Banner)
admin.site.register(Testimonial)
admin.site.register(Subscribe)
