Vue.component('modallead', {
    methods: {
        closeModal(e){
            if(Boolean(this.close)){
              this.close();
            }
        }
    },
    template:
    `
    <transition name="modal">
        <div class="modal-mask modal-lead">
            <div @click="$emit('close')" class="modal-wrapper">
                <div class="modal-container"  @click.stop>
                    <i @click="$emit('close')" class="close fas fa-times"></i>
                    <div class="modal-body">
                        <slot name="body">
                        </slot>
                    </div>
                </div>
            </div>
        </div>
    </transition>
    `
})
