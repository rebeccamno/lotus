import threading
import json

from django.conf import settings

from mailchimp3 import MailChimp
from mailchimp3.helpers import get_subscriber_hash

from django.template.loader import get_template
from django.utils.safestring import mark_safe

from django.core.mail import EmailMessage


class SendEmail(object):

    def __init__(self, title, context, template, users):
        self.title = title
        self.context = context
        self.template = template
        self.users = users
        if settings.DEBUG:
            self.title += ' - EMAIL DE TESTE'
            self.users = settings.DEV_USERS

    def get_content(self):
        content = get_template(self.template).render(self.context)
        return mark_safe(content)

    def send(self):
        email = EmailMessage(self.title, self.get_content(), from_email=settings.SERVER_EMAIL, to=self.users)
        email.content_subtype = 'html'
        email.send()


class SubscribeMailChimp(object):
    def __init__(self, subscribe, update=False):
        self.email = subscribe.email
        self.name = subscribe.name
        self.phone = subscribe.phone
        self.type = subscribe.type
        self.type_slug = subscribe.type_slug
        self.tag = subscribe.tag
        self.update = update
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def customer_to_mailchimp_member(self):

        return {
            'status': 'subscribed',
            'email_address': self.email,
            'merge_fields': {
                'FNAME': self.name,
                'Email': self.email,
                'PHONE': self.phone
            }
        }

    def add_tag(self, list_id, client):
        subscriber_hash = get_subscriber_hash(self.email)
        data = {
            'tags': [
                {'name': self.tag, 'status': 'active'},
            ]
        }

        try:
            client.lists.members.tags.update(list_id=list_id, subscriber_hash=subscriber_hash, data=data)
        except Exception as e:
            print(e)
            return False

    def run(self):
        LIST_ID = settings.MAILCHIMP_API_LIST
        client = MailChimp(settings.MAILCHIMP_API_KEY)
        member = self.customer_to_mailchimp_member()

        try:
            if not self.update:
                client.lists.members.create(LIST_ID, member)
            self.add_tag(LIST_ID, client)
        except Exception as e:
            print(e)
            return False
