from django.db import models
from ckeditor.fields import RichTextField


class WebsiteInfo(models.Model):
    website_name = models.CharField("Nome", max_length=255)
    history = models.TextField("Texto Sobre a Lótus")
    mission = models.TextField("Texto Missão")
    values = models.TextField("Texto Valores")
    cnpj = models.CharField("CNPJ", max_length=50, blank=True)
    razao_social = models.CharField("Razão Social", max_length=50, blank=True)
    facebook = models.URLField("Facebook", blank=True)
    instagram = models.URLField("Instagram", blank=True)
    whatsapp_link = models.URLField("Whatsapp Link", blank=True)
    address = models.CharField("Endereço", max_length=255, blank=True)
    phones = models.CharField("Telefones", max_length=255, blank=True)
    contact_image = models.ImageField("Imagem Contato", upload_to='website/contact/', blank=True)

    class Meta:
        verbose_name = ('Website')
        verbose_name_plural = ('Website')

    def __str__(self):
        return "Lótus"

class Testimonial(models.Model):
    name = models.CharField("Nome", max_length=50)
    image = models.ImageField("Imagem", upload_to='website/testimonials/', blank=True)
    text = models.CharField("Texto", max_length=255)
    description = models.CharField("Descrição", max_length=50, blank=True)

    class Meta:
        verbose_name = ('Depoimento')
        verbose_name_plural = ('Depoimentos')

    def __str__(self):
        return self.name


class Banner(models.Model):
    image = models.ImageField("Imagem", upload_to='website/banner/images/')
    subtitle = models.CharField("Subtítulo", max_length=50, blank=True)
    title = models.CharField("Título", max_length=50, blank=True)
    button_label = models.CharField("Label do botão", max_length=50, blank=True)
    button_url = models.URLField("Url do botão", blank=True)
    order = models.IntegerField(default=0)

    class Meta:
        verbose_name = ('Banner')
        verbose_name_plural = ('Banner')

    def __str__(self):
        return "Banner - {}".format(self.title)


class Feature(models.Model):
    name = models.CharField("Nome", max_length=255)
    title = models.CharField("Subtítulo", max_length=255, blank=True)
    text = RichTextField(verbose_name="Texto", blank=True)
    tour_duration = models.CharField("Duração do passeio", blank=True, max_length=255)
    text2 = RichTextField(verbose_name="Texto 2", blank=True)
    image = models.ImageField("Imagem", upload_to='website/features/')
    whatsapp_link = models.URLField(blank=True)
    order = models.IntegerField(default=0)
    slug = models.SlugField()

    class Meta:
        verbose_name = ('Serviço')
        verbose_name_plural = ('Serviços')

    def __str__(self):
        return self.name


class FeaturePrice(models.Model):
    feature  = models.ForeignKey(Feature, related_name="prices", on_delete=models.CASCADE)
    name = models.CharField("Label", max_length=255)
    price = models.DecimalField("Preço", max_digits=6, decimal_places=2)
    obs = models.TextField("Observação", blank=True)

    class Meta:
        verbose_name = ('Preço')
        verbose_name_plural = ('Preços')

    def __str__(self):
        return self.name


class FeatureImage(models.Model):
    feature = models.ForeignKey(Feature, related_name="images", on_delete=models.CASCADE)
    image = models.ImageField("Imagem", upload_to='website/features/images/')

    class Meta:
        verbose_name = ('Imagem')
        verbose_name_plural = ('Imagens')

    def __str__(self):
        return self.feature.name


class FeatureTestimonialImage(models.Model):
    feature = models.ForeignKey(Feature, related_name="testimonials_images", on_delete=models.CASCADE)
    image = models.ImageField("Imagem", upload_to='website/features/testimonials-images/')

    class Meta:
        verbose_name = ('Imagem Depoimento')
        verbose_name_plural = ('Depoimentos Imagens')

    def __str__(self):
        return self.feature.name


class FeaturePlace(models.Model):
    feature = models.ForeignKey(Feature, related_name="places", on_delete=models.CASCADE)
    name = models.CharField("Nome", max_length=255)

    class Meta:
        verbose_name = ('Lugar')
        verbose_name_plural = ('Lugares')

    def __str__(self):
        return self.feature.name


class TripInfo(models.Model):
    RJ = 'RJ'
    SP = 'SP'
    PLACE_CHOICES = (
        (RJ, 'RJ'),
        (SP, 'SP'),
    )
    title = models.CharField("Título", max_length=255)
    subtitle = models.CharField("Subtítulo", max_length=255)
    highlight = models.CharField(verbose_name="Texto destacado", max_length=255, blank=True)
    included_in_title = models.CharField("Título dois", max_length=255, default="O que está incluso em sua Viagem")
    sale_text = RichTextField(verbose_name="Texto venda", blank=True)
    obs  = RichTextField(verbose_name="Observação", blank=True)
    payment_information =  RichTextField(verbose_name="Informações de pagamento", blank=True)
    place = models.CharField(
        'Local',
        max_length=2,
        choices=PLACE_CHOICES,
        default=RJ,
    )
    slug = models.SlugField()

    class Meta:
        verbose_name = ('Trip')
        verbose_name_plural = ('Trip\'s')

    def __str__(self):
        return self.title


class TripIncludedItem(models.Model):
    trip = models.ForeignKey(TripInfo, related_name="included_items", on_delete=models.CASCADE)
    text = models.CharField("Texto", max_length=255, blank=True)

    class Meta:
        verbose_name = ('Item')
        verbose_name_plural = ('Itens incluidos na viagem')

    def __str__(self):
        return self.text


class TripPrice(models.Model):
    trip = models.ForeignKey(TripInfo, related_name="prices", on_delete=models.CASCADE)
    name = models.CharField("Label", max_length=255)
    price = models.DecimalField("Preço", max_digits=6, decimal_places=2)

    class Meta:
        verbose_name = ('Preço')
        verbose_name_plural = ('Preços')

    def __str__(self):
        return self.name

class TripDate(models.Model):
    trip = models.ForeignKey(TripInfo, related_name="dates", on_delete=models.CASCADE)
    name = models.CharField("Label", max_length=255, blank=True)
    date = models.DateField("Data")

    class Meta:
        verbose_name = ('Data')
        verbose_name_plural = ('Datas')

    def __str__(self):
        return self.name

class TripTestimonialImage(models.Model):
    trip = models.ForeignKey(TripInfo, related_name="testimonials_images", on_delete=models.CASCADE)
    image = models.ImageField("Imagem", upload_to='website/trips/testimonials-images/')

    class Meta:
        verbose_name = ('Imagem Depoimento')
        verbose_name_plural = ('Depoimentos Imagens')

    def __str__(self):
        return self.trip.title


class TripImage(models.Model):
    trip = models.ForeignKey(TripInfo, related_name="images", on_delete=models.CASCADE)
    image = models.ImageField("Imagem", upload_to='website/trips/images/')

    class Meta:
        verbose_name = ('Imagem')
        verbose_name_plural = ('Imagens')

    def __str__(self):
        return self.trip.title


class Subscribe(models.Model):
    CONTACT = 'contact'
    TRIP = 'trip'
    FEATURE = 'feature'
    TYPE_CHOICES = (
        (CONTACT, 'Contato'),
        (TRIP, 'Trips'),
        (FEATURE, 'Serviços')
    )
    email = models.EmailField()
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
    type = models.CharField(
        'Tipo',
        max_length=20,
        choices=TYPE_CHOICES,
        default=CONTACT,
    )
    type_slug = models.CharField(max_length=255, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email

    @property
    def tag(self):
        tag = 'geral'

        if self.type and self.type_slug:
            if self.type == 'feature':
                tag = Feature.objects.get(slug=self.type_slug).name
            elif self.type == 'trip':
                tag = "Trip - " + TripInfo.objects.get(slug=self.type_slug).place

        return tag
