
//Vue.component(VueCountdown.name, VueCountdown);
Vue.use(VueAwesomeSwiper)
Vue.directive('mask', VueMask.VueMaskDirective);

new Vue({
    el: '#app',
    mixins: [],
    data() {
        return {
          mobileMenuOpen: false,
          showModal: false,
          showModalLead: false,
          leadType: null,
          leadSlug: null,
          leadTypeName: null,
          showModalTrips: false,
          showModalGallery: false,
          modalFeature: null,
          modalGalleryImages: [],
          features:[],
          banners:[],
          testimonials: [],
          history: null,
          mission: null,
          values: null,
          cnpj: null,
          razao_social: null,
          facebook: null,
          instagram: null,
          whatsapp_link: null,
          phones: null,
          address: null,
          contact_image: null,
          contact_name: null,
          contact_phone: null,
          contact_email: null,
          successContact: false,
          lead_contact_name: null,
          lead_contact_phone: null,
          lead_contact_email: null,
          successLead: false,
          errors: false,
          lead_errors: false,
          bannerSwiperOptions:{
            spaceBetween: 30,
            effect: 'fade',
            slidesPerView: 1,
            pagination: {
              el: '.swiper-pagination'
            },
            autoplay: {
              delay: 2500,
              disableOnInteraction: true,
            },
          },
          imagesSwiperOptions:{
            spaceBetween: 20,
            slidesPerView: 4,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
            breakpoints: {
              640: {
                slidesPerView: 1,
                spaceBetween: 10
              }
            }
          },
          featureTestimonialImagesSwiperOptions:{
            spaceBetween: 20,
            slidesPerView: 4,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
            breakpoints: {
              640: {
                slidesPerView: 1,
                spaceBetween: 10
              }
            }
          },
          tripsTestimonialImagesSwiperOptions:{
            spaceBetween: 20,
            slidesPerView: 4,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
            breakpoints: {
              640: {
                slidesPerView: 1,
                spaceBetween: 10
              }
            }
          },
          tripsImagesSwiperOptions:{
            spaceBetween: 20,
            slidesPerView: 4,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
            breakpoints: {
              640: {
                slidesPerView: 1,
                spaceBetween: 10
              }
            }
          },
          testimonialsSwiperOptions:{
            spaceBetween: 50,
            autoHeight: true,
            slidesPerView: 1,
            pagination: {
              el: '.testimonial-swiper-pagination'
            },
            navigation: {
              nextEl: '.testimonial-swiper-button-next',
              prevEl: '.testimonial-swiper-button-prev'
            },
          },
          featuresSwiperOptions: {
            slidesPerView: 3,
            pagination: {
              el: '.swiper-pagination'
            },
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
            breakpoints: {
              640: {
                slidesPerView: 1,
                spaceBetween: 10
              }
            }
         },
         galleryImagesSwiperOptions:{
           spaceBetween: 10,
           slidesPerView: 1,
           navigation: {
             nextEl: '.swiper-button-next',
             prevEl: '.swiper-button-prev'
           },
         },
        }
    },
    components: {
      LocalSwiper: VueAwesomeSwiper.swiper,
      LocalSlide: VueAwesomeSwiper.swiperSlide
    },
    computed: {
      featuresSwiper() {
        return this.$refs.featuresSwiper.swiper
      },
      imagesSwiper() {
        return this.$refs.imagesSwiper.swiper
      },
      bannerSwiper() {
        return this.$refs.bannerSwiper.swiper
      },
      testimonialsSwiper(){
        return this.$refs.testimonialsSwiper.swiper
      },
      featureTestimonialImagesSwiper(){
        return this.$refs.featureTestimonialImagesSwiper.swiper
      },
      tripsTestimonialImagesSwiper(){
        return this.$refs.tripsTestimonialImagesSwiper.swiper
      },
      tripsImagesSwiper(){
        return this.$refs.tripsImagesSwiper.swiper
      },
      galleryImagesSwiper(){
        return this.$refs.galleryImagesSwiper.swiper
      },
      formatDate(){
        var vm = this;
        return function (date) {
          let d = date.split("-")
          d = new Date(d[0], d[1], d[2]);
          let month = '' + (d.getMonth() )
          let day = '' + d.getDate()
          let year = d.getFullYear()
          return day + "/" + month + "/" + year;
        };
      }
    },
    mounted() {
      this.getData();

    },
    methods: {
      getPage(){
        const PARAMS = new URLSearchParams(window.location.search);
        const PAGE = PARAMS.get('page');
        return PAGE
      },
      getTripPage(){
        const PARAMS = new URLSearchParams(window.location.search);
        const TRIP = PARAMS.get('trip');
        return TRIP
      },
      getData(){
        let headers = new Headers();

        let config = { method: 'GET',
                       headers: headers,
                       mode: 'cors',
                       cache: 'default' };

        const URL = 'https://admin.lotusturismoarraial.com.br/api/v1/';

        fetch(URL, config) .then((response)=>{
          response.json().then((data)=>{
            this.testimonials = data.testimonials
            this.features = data.features
            this.history = data.history
            this.mission = data.mission
            this.values = data.values
            this.trips = data.trips
            this.banners = data.banners
            this.cnpj = data.cnpj
            this.razao_social =  data.razao_social
            this.facebook = data.facebook
            this.instagram = data.instagram
            this.whatsapp_link = data.whatsapp_link
            this.phones = data.phones
            this.address = data.address
            this.contact_image = data.contact_image


            const PAGE = this.getPage()
            const PAGE_TRIP = this.getTripPage()
            if (PAGE){
              const FEATURE = this.features.find( feature => feature.slug === PAGE );
              if(FEATURE){
                this.openModal(FEATURE)
              }
            }
            if (PAGE_TRIP){
              const TRIP = this.trips.find( trip => trip.slug === PAGE_TRIP );
              if(TRIP){
                this.openModalTrips(TRIP)
              }
            }

          }).catch((error) => {
            console.log("error", error);
          });
        })
        .catch(function(err) {
          console.error(err);
        });
      },
      validEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      },
      checkForm(e){
        e.preventDefault();
        this.errors = {};

        if (!this.contact_name) {
          this.errors['contact_name'] = 'O nome é obrigatório.'
        }
        if (!this.contact_phone) {
          this.errors['contact_phone'] = 'O telefone é obrigatório.'
        }

        if (!this.contact_email) {
          this.errors['contact_email'] = 'O email é obrigatório.'
        } else if (!this.validEmail(this.contact_email)) {
          this.errors['contact_email'] = 'Utilize um e-mail válido.'
        }

        if (!Object.keys(this.errors).length) {
          this.sendContact();
       }
      },
      checkLeadForm(e){
        e.preventDefault();
        this.lead_errors = {};
        if (!this.lead_contact_name) {
          this.lead_errors['lead_contact_name'] = 'O nome é obrigatório.'
        }
        if (!this.lead_contact_phone) {
          this.lead_errors['lead_contact_phone'] = 'O telefone é obrigatório.'
        }

        if (!this.lead_contact_email) {
          this.lead_errors['lead_contact_email'] = 'O email é obrigatório.'
        } else if (!this.validEmail(this.lead_contact_email)) {
          this.lead_errors['lead_contact_email'] = 'Utilize um e-mail válido.'
        }
        console.log(this.lead_errors);
        if (!Object.keys(this.lead_errors).length) {
          this.sendLead();
       }
      },
      sendLead(){
        let headers = new Headers();
        let data = {
          'name': this.lead_contact_name,
          'phone': this.lead_contact_phone,
          'email': this.lead_contact_email,
          'type': this.leadType,
          'type_slug': this.leadSlug
        }

        let config = { method: 'POST',
                      body: JSON.stringify(data),
                      headers: headers,
                      mode: 'cors',
                      cache: 'default' };

        const URL = 'https://admin.lotusturismoarraial.com.br/api/v1/contact/';

        fetch(URL, config) .then((response)=>{
            if(response.status === 200){
              this.lead_contact_name = null
              this.lead_contact_phone = null
              this.lead_contact_email = null
              this.leadType = null
              this.successLead = true

            }else{
              this.successLead = false
            }

        })
        .catch(function(err) {
          alert("Ocorreu um erro, tente novamente!")
          console.error(err);
        });
      },
      sendContact(){
        let headers = new Headers();
        let data = {
          'name': this.contact_name,
          'phone': this.contact_phone,
          'email': this.contact_email
        }

        let config = { method: 'POST',
                      body: JSON.stringify(data),
                      headers: headers,
                      mode: 'cors',
                      cache: 'default' };

        const URL = 'https://admin.lotusturismoarraial.com.br/api/v1/contact/';

        fetch(URL, config) .then((response)=>{
            if(response.status === 200){
              this.contact_name = null
              this.contact_phone = null
              this.contact_emai = null
              this.successContact = true

            }else{
              this.successContact = false
            }

        })
        .catch(function(err) {
          alert("Ocorreu um erro, tente novamente!")
          console.error(err);
        });
      },
      toggleMenu(){
        this.mobileMenuOpen = !this.mobileMenuOpen
      },
      openModal(feature) {
        this.showModal = true
        this.modalFeature = feature
        fbq('track', 'ViewContent', {
           content_ids: [feature.id],
           content_name: feature.name,
           content_type: 'product'
         });
         gtag('event', 'click', {
           'event_category': 'Conteúdo',
           'event_label': feature.name
         })
      },
      closeModal() {
          this.showModal = false
          this.modalFeature = null
      },
      openModalTrips(trip) {
        this.modalTrip = trip
        this.showModalTrips = true
      },
      closeModalTrips() {
          this.showModalTrips = false
      },

      openModalGallery(index, images) {
        this.galleryImagesSwiperOptions.initialSlide = index
        this.modalGalleryImages = images
        this.showModalGallery = true

      },
      openLeadModal(leadType, leadSlug, leadTypeName){
        this.leadType = leadType
        this.leadSlug = leadSlug
        this.leadTypeName = leadTypeName
        this.showModalLead = true
        this.successLead = false
      },
      closeModalLead(){
        this.leadType = null
        this.leadSlug = null
        this.leadTypeName = null
        this.showModalLead = false
      },
      closeModalGallery() {
          this.showModalGallery = false
          this.modalGalleryImages = []
      },
      trackZap(texto) {
        fbq('trackCustom', 'Zap '+texto);
        gtag('event', 'click', {
          'event_category': 'Engajamento',
          'event_label': 'Zap '+texto
        });
      }
    },
})
