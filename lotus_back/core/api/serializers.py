from rest_framework import serializers

from core.models import (Banner, Feature, FeatureImage, FeaturePlace, FeaturePrice,
                         Testimonial, TripImage, TripDate, TripInfo, TripIncludedItem,
                         FeatureTestimonialImage, TripTestimonialImage, WebsiteInfo)


class FeatureImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeatureImage
        fields = ('image',)

class FeatureTestimonialImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeatureTestimonialImage
        fields = ('image',)

class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banner
        fields = ('__all__')


class TestimonialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Testimonial
        fields = ('__all__')


class FeaturePriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeaturePrice
        fields = ('__all__')


class FeaturePlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeaturePlace
        fields = ('name',)


class TripTestimonialImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripTestimonialImage
        fields = ('image',)


class TripImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripImage
        fields = ('image',)


class FeatureSerializer(serializers.ModelSerializer):
    images = FeatureImageSerializer(many=True, read_only=True)
    prices = FeaturePriceSerializer(many=True, read_only=True)
    places = FeaturePlaceSerializer(many=True, read_only=True)
    testimonials_images = FeatureTestimonialImageSerializer(many=True, read_only=True)

    class Meta:
        model = Feature
        fields = ('__all__')


class TripDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripDate
        fields = ('name', 'date',)


class TripIncludedItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripIncludedItem
        fields = ('text', )


class TripInfoSerializer(serializers.ModelSerializer):
    dates = TripDateSerializer(many=True, read_only=True)
    included_items = TripIncludedItemSerializer(many=True, read_only=True)
    testimonials_images = TripTestimonialImageSerializer(many=True, read_only=True)
    images = TripImageSerializer(many=True, read_only=True)

    class Meta:
        model = TripInfo
        fields = ('__all__')


class WebsiteInfoSerializer(serializers.ModelSerializer):
    features = serializers.SerializerMethodField()
    testimonials = serializers.SerializerMethodField()
    trips = serializers.SerializerMethodField()
    banners = serializers.SerializerMethodField()

    def get_features(self, obj):
        features = Feature.objects.all().order_by('order')
        serializer = FeatureSerializer(features, many=True, read_only=True, context={
                                       'request': self.context['request']})
        return serializer.data

    def get_testimonials(self, obj):
        testimonials = Testimonial.objects.all()
        serializer = TestimonialSerializer(testimonials, many=True, read_only=True, context={
                                       'request': self.context['request']})
        return serializer.data

    def get_trips(self, obj):
        trips = TripInfo.objects.all()
        serializer = TripInfoSerializer(trips, many=True, read_only=True, context={
                                       'request': self.context['request']})
        return serializer.data

    def get_banners(self, obj):
        banner = Banner.objects.all().order_by('order')
        serializer = BannerSerializer(banner, many=True, read_only=True, context={
                                       'request': self.context['request']})
        return serializer.data

    class Meta:
        model = WebsiteInfo
        fields = ('__all__')
