from django.urls import path

from .views import (
    contact_post,
    FeatureListAPIView,
    WebsiteInfoAPIView
)

urlpatterns = [
    path('', WebsiteInfoAPIView.as_view(), name='websiteinfo'),
    path('contact/', contact_post, name='contact'),
    path('servicos/', FeatureListAPIView.as_view(), name='features'),
]
