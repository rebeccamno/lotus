import json

from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from core.utils import SendEmail, SubscribeMailChimp
from core.api.serializers import FeatureSerializer, WebsiteInfoSerializer
from core.models import Feature, WebsiteInfo, Subscribe, TripInfo


@require_http_methods(["POST"])
@csrf_exempt
def contact_post(request):

    if request.method == 'POST':
        request_json = json.loads(request.body)
        email = request_json['email']
        name = request_json['name']
        phone = request_json['phone']
        type = request_json.get('type', 'contact')
        type_slug = request_json.get('type_slug', None)

        update = False

        email_qs = Subscribe.objects.filter(email=email)

        if email_qs.exists():
            update = True

        subscribe = Subscribe.objects.create(
            email=email,
            name=name,
            phone=phone,
            type=type,
            type_slug=type_slug
        )


        SubscribeMailChimp(subscribe, update=update)


    # email = SendEmail(
    #     'LÓTUS - CONTATO',
    #     {
    #         'data': request_json
    #     },
    #     'contact_email.html',
    #     [settings.SERVER_EMAIL]
    # )
    #email.send()
    return JsonResponse({"status": "OK"})


class FeatureListAPIView(ListAPIView):
    serializer_class = FeatureSerializer
    queryset = Feature.objects.all()


class WebsiteInfoAPIView(APIView):

    def get(self, request, format=None):
        website_info = WebsiteInfo.objects.first()

        return Response(WebsiteInfoSerializer(website_info, many=False, context={'request': request}).data)
